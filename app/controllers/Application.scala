package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.db._
import anorm._
import anorm.SqlParser._
import play.api.Play.current

import views._

case class User(id:Pk[Long] = NotAssigned,uname:String,uage:Int)
object Application extends Controller {
  //save user form defintion
  val saveUserForm = Form {
    mapping(
      "id" -> ignored(NotAssigned:Pk[Long]),
      "uname" -> nonEmptyText,
      "uage" -> number(min=0,max=100)
    )(User.apply)(User.unapply)
  }

  def index = Action {implicit request =>
    DB.withConnection{implicit conn =>
      val users= SQL("select *from User").as(get[Pk[Long]]("id") ~ get[String]("uname") ~ get[Int]("uage") map {case id ~ uname ~ uage => User(id,uname,uage)} *)
      Ok(html.index(users))
    }
  }

  def add = Action {
    Ok(html.add(saveUserForm))
  }

  def save = Action { implicit request =>
    saveUserForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.add(formWithErrors)),
      user => {
        DB.withConnection{implicit conn =>
          SQL("insert into User(uname,uage) values({uname},{uage})").on('uname -> user.uname,'uage -> user.uage).executeUpdate()
        }
        Redirect("/")
      }
    )
  }

  def edit(id:Long) = TODO
  def delete(id:Long) = Action {
    DB.withConnection{implicit conn =>
      SQL("delete from User where id = {id}").on("id" -> id).executeUpdate()
      Redirect("/").flashing("success" -> "delete success")
    }
  }

}